import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ColorServiceService } from '../color-service.service';
import { MatSnackBar } from '@angular/material'

export interface Food {
  value: string;
  viewValue: string;
}
interface myData {
  obj: any
}

@Component({
  selector: 'main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class MainScreenComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isOptional = false
  constructor(private _formBuilder: FormBuilder, private http: HttpClient,
    public service: ColorServiceService, public snackBar: MatSnackBar) { }
   
  public primerColor: any = '';
  public segundoColor: any = '';
  resultingColor: any = '';
  resultingColorHexa: any = '';
  colors = [
    {
      name: 'Amarillo',
      nameFront: 'Amarillo',
      value: '#FAE405'
    },
    {
      name: 'Rojo',
      nameFront: 'Rojo',
      value: '#ff3300'
    },
    {
      name: 'Azul',
      nameFront: 'Azul',
      value: '#0000FF'
    },
    {
      name: 'Verde',
      nameFront: 'Verde',
      value: '#088A29'
    },
    {
      name: 'Naranja',
      nameFront: 'Naranja',
      value: '#FFA000'
    },
    {
      name: 'Morado',
      nameFront: 'Morado',
      value: '#660099'
    }

  ];
  colors2 = [
    {
      name: 'Amarillo',
      nameFront: 'Amarillo',
      value: '#FAE405'
    },
    {
      name: 'Rojo',
      nameFront: 'Rojo',
      value: '#ff3300'
    },
    {
      name: 'Azul',
      nameFront: 'Azul',
      value: '#0000FF'
    },
    {
      name: 'Verde',
      nameFront: 'Verde',
      value: '#088A29'
    },
    {
      name: 'Naranja',
      nameFront: 'Naranja',
      value: '#FFA000'
    },
    {
      name: 'Morado',
      nameFront: 'Morado',
      value: '#660099'
    }
  ];

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  getVlue() {
    this.service.getResult(this.primerColor, this.segundoColor).subscribe((data) => {
      this.resultingColor = data.colorResultante;
      this.resultingColorHexa = data.colorHexa;
       
    });
  }




}
