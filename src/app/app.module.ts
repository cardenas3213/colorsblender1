import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatCardModule,
MatToolbarModule, MatTabsModule, MatStepperModule, MatListModule, MatSnackBarModule, MatChipsModule,
MatExpansionModule} from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import {ColorServiceService} from './color-service.service';
import {RouterModule, Routes} from '@angular/router';
import { HomeScreenComponent } from './home/home-screen/home-screen.component';


const routes : Routes = [
  {path:'colors', component: MainScreenComponent},
  {path:'home', component:HomeScreenComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    MainScreenComponent,
    HeaderComponent,
    HomeScreenComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, MatButtonModule, FormsModule, MatInputModule,
    MatFormFieldModule, MatSelectModule, MatCardModule, MatToolbarModule, MatTabsModule,MatStepperModule,
    ReactiveFormsModule, HttpClientModule, MatListModule, RouterModule.forRoot(routes),
    MatSnackBarModule, MatChipsModule, MatExpansionModule
  ],
  providers: [BrowserAnimationsModule, FormsModule, ColorServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
